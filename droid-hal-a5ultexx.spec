%define device a5ultexx
%define vendor samsung

%define vendor_pretty Samsung
%define device_pretty Galaxy A5 2015

%define installable_zip 1

%define enable_kernel_update 1

%define makefstab_skip_entries /dev/cpuctl /dev/stune

%define straggler_files \
/init.class_main.sh\
/init.qcom.early_boot.sh\
/init.qcom.sh\
/init.qcom.usb.sh\
/selinux_version\
/service_contexts\
/file_contexts.bin\
/property_contexts\
/sdcard\
/d\
/vendor\
/bugreports\
%{nil}

%define android_config \
#define QCOM_BSP 1\
#define QTI_BSP 1\
%{nil}
%define __provides_exclude_from ^/system/.*$
%define __requires_exclude ^/system/bin/.*$
%define __find_provides %{nil}
%define __find_requires %{nil}
%define additional_post_scripts \
/usr/bin/groupadd-user inet || :\
%{nil}

%include rpm/dhd/droid-hal-device.inc
